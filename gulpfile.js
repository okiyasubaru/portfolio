//  define variables
const gulp = require("gulp"),
  glob = require("glob"),
  nunjucksRender = require("gulp-nunjucks-render"),
  autoprefixer = require("gulp-autoprefixer"),
  csso = require("gulp-csso"),
  sass = require("gulp-sass")(require("node-sass")),
  sourcemaps = require("gulp-sourcemaps"),
  purgecss = require("gulp-purgecss"),
  concat = require("gulp-concat"),
  browser = require("browser-sync");

//  nunjucks task
gulp.task("nunjucks", function () {
  const includeDirs = ["src/html/", "src/html/partials/"];

  return gulp
    .src("src/html/*.+(html|nj)")
    .pipe(
      nunjucksRender({
        path: includeDirs.reduce(function (dirs, g) {
          return dirs.concat(glob.sync(g));
        }, []),
      })
    )
    .pipe(gulp.dest("dist"));
});

//  sass task
const filesScss = [
  "src/sass/main.sass",
  // "src/sass/layout/global.sass",
  // "src/sass/layout/colors.sass",
  // "src/sass/pages/home.sass",
];
gulp.task("compileScss", () => {
  return gulp
    .src(filesScss)
    .pipe(sourcemaps.init())
    .pipe(sass().on("error", sass.logError))
    .pipe(autoprefixer())
    .pipe(concat("main.css"))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest("dist/css"))
    .pipe(browser.stream());
});
gulp.task("compileMinifyScss", () => {
  return gulp
    .src(filesScss)
    .pipe(sass().on("error", sass.logError))
    .pipe(autoprefixer())
    .pipe(csso())
    .pipe(
      purgecss({
        content: ["src/html/**/*.+(html|nj)"],
      })
    )
    .pipe(concat("main.min.css"))
    .pipe(gulp.dest("dist/css"));
});

//  reload task
function reload(cb) {
  browser.reload();
  cb();
}
gulp.task("serve", () => {
  browser.init({
    server: "./dist",
    https: false,
    injectChanges: true,
  });

  gulp.watch(["src/html/**/*", "src/html/*"], gulp.series("nunjucks", reload));
  gulp.watch(
    filesScss,
    gulp.series("compileScss", "compileMinifyScss", reload)
  );
});

gulp.task(
  "default",
  gulp.series(
    gulp.parallel("nunjucks", "compileScss", "compileMinifyScss"),
    "serve"
  )
);